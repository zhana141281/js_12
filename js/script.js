const images = document.querySelectorAll('img');
console.log(images);

const divButton = document.querySelector('.button');
console.log(divButton);


images.forEach(function (el) {
    el.hidden = true;

});
localStorage.setItem('id', '0');
images[localStorage.getItem('id')].hidden = false;


let i = localStorage.getItem('id');
let timer;
function showImg () {
    timer = setInterval(function () {
        images.forEach(function (el) {
            el.hidden = true;
        });
        images[i].hidden = false;
        localStorage.setItem('id', i);
        i++;
        if (i >= images.length)
        i = 0;
    }, 2000);

}
showImg();



divButton.addEventListener('click', function (event) {
    if (event.target.className === 'start') {
       showImg()
    } else {
        clearInterval(timer);
    }
})